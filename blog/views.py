from django.shortcuts import render, render_to_response, get_object_or_404
from django.http.response import HttpResponse
from .models import Post


# Create your views here.
def index(request):

    # get all the posts
    posts = Post.objects.all()

    # return HttpResponse("Hey there...")
    # using render for the views
    return render(request, 'index.html', {'posts': posts})


def post(request, slug):
    print(slug)
    # get all the posts
    posts = Post.objects.all()

    # return HttpResponse("hey, this is my post page")
    return render_to_response('post.html', {
        # sending the objects to the post page as a response object
        'post': get_object_or_404(Post, slug=slug), # single post
        'posts': posts # sending all the posts
    })


def about(request):
    return render(request, 'about.html', {})

