1- run python ./manage.py makemigrations blog

2- run python ./manage.py makemigrations --fake blog

3- add the new field in database
	image = models.ImageField(upload_to='img')  # sends the images to media/img

4- rerun python ./manage.py makemigrations blog

5- run python ./manage.py migrate blog

the system will recognize that existes a new field, and provide a fix
select the default value for all the previous post and all the future
posts that don't upload any image.